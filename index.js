const { clear } = require('console');

const app = require('./config/express')();
const port = app.get('port')

app.listen(port, () => {
    console.clear()
    console.log(`\nApi running at http://localhost:${port}/`);
});