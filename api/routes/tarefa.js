module.exports = app => {

    const controller = app.controllers.tarefa;

    app.route('/tarefa')
        .get(            
            controller.getAllTask            
        ) 
        .post(
            controller.addTask
        )
    
    app.route('/tarefa/:id')
        .get(
            controller.getTaskById
        )
        .put(
            controller.editTask
        )
        .delete(
            controller.deleteTask
        )
}