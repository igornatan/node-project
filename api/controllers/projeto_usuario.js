module.exports = app => {
    
    const controller = {};

    controller.addProject = function(req, res){        
        const req_user = req.body.id_usuario
        const id_projeto = req.body.id_projeto;
        const values = [id_projeto, id_usuarios]

        app.db.none('INSERT INTO projeto_usuario (id_projeto, id_usuario) VALUES($1, $2)', values).then(data => {
            res.status(200).json('Projeto inserido para os usuários');
        }).catch(function (err){
            return next(err);
        });
    }

    controller.getAllProjects = function(req, res, next){
        app.db.any('SELECT * FROM projeto_usuario')
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data,
                        message: 'Todos os usuarios'
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.getProjectById = function(req, res, next){
        const id = parseInt(req.params.id);
        app.db.any('SELECT * FROM projeto_usuario WHERE id_projeto = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.deleteProjectUser = async function(req, res, next){
        const id = parseInt(req.params.id);
        app.db.any('DELETE FROM projeto_usuario WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        message: 'Projeto e usuario desvinculados'
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.editProjectUser  = function(req, res){
        const req_user = req.body.id_usuario
        const id_projeto = req.body.id_projeto;
        const id = parseInt(req.params.id); 

        app.db.none('UPDATE projeto_usuario SET id_projeto = $1, id_usuario = $2 where id = $3', [id_projeto, id_usuario, id]).then(data => {
            res.status(200).json('Editado com sucesso!');
        }).catch(function (err){
            return next(err);
        });
    }
    
    return controller;
}